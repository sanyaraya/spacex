//
//  Rocket.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Rocket: Decodable {
    
    //MARK: - Constants
    private enum CodingKeys: String, CodingKey {
        case id
        case country
        case company
        case description
        case name = "rocket_name"
        case wikiLink = "wikipedia"
        case images = "flickr_images"
    }
    
    //MARK: - Properties
    
    let id: Int
    let name: String
    let country: String
    let company: String
    let description: String
    let wikiLink: String
    let images: [String]
  
}

extension Rocket: ItemPresentable {
    var title: String {
        return self.name
    }
    
    var imageUrl: String {
        return self.images.first ?? ""
    }
    
    var infoLink: String {
        return self.wikiLink
    }
}
