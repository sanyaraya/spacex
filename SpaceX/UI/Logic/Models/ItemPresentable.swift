//
//  ItemPresentable.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

protocol ItemPresentable {
    var title: String {get}
    var imageUrl: String {get}
    var infoLink: String {get}
}
