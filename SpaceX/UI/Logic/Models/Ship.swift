//
//  Ship.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Ship: Decodable {
    
    //MARK: - Constants
    private enum CodingKeys: String, CodingKey {
        case id = "ship_id"
        case name = "ship_name"
        case url
        case image
        case homePort = "home_port"
    }
    
    //MARK: - Properties
    
    let id: String
    let name: String
    let homePort: String
    let url: String
    let image: String
}

extension Ship: ItemPresentable {
    var title: String {
        return self.name
    }
    
    var imageUrl: String {
        return self.image
    }
    
    var infoLink: String {
        return self.url
    }
}
