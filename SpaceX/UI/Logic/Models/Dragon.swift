//
//  Dragon.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Dragon: Decodable {
    //MARK: - Constants
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case description
        case name
        case wikiLink = "wikipedia"
        case images = "flickr_images"
    }
    
    //MARK: - Properties
    
    let id: String
    let name: String
    let type: String
    let description: String
    let wikiLink: String
    let images: [String]
}


extension Dragon: ItemPresentable {
    var title: String {
        return self.name
    }
    
    var imageUrl: String {
        return self.images.first ?? ""
    }
    
    var infoLink: String {
        return self.wikiLink
    }
}
