//
//  DataManager.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import RxAlamofire
import MoreCodable

class DataManager: NSObject {

    //MARK: - Properties
    
    static let shared = DataManager()
    
    let manager = SessionManager.default
    
    //MARK: - Constants
    
    let baseUrl = "https://api.spacexdata.com/v3/"
    
    enum Packages {
        static let rockets = "rockets"
        static let ships = "ships"
        static let dragons = "dragons"
    }
    
    //MARK: - Methods
    
    func getDragons() -> Observable<[ItemPresentable]> {
        return self.executeURL(method: .get, url: baseUrl + Packages.dragons, params: [:]).map({ (req, res)  in
            self.parseObjects(json: res, type: Dragon.self)
        })
    }
    
    
    func getShips() -> Observable<[ItemPresentable]> {
        return self.executeURL(method: .get, url: baseUrl + Packages.ships, params: [:]).map({ (req, res)  in
            self.parseObjects(json: res, type: Ship.self)
        })
    }
    
    
    func getRockets() -> Observable<[ItemPresentable]> {
        return self.executeURL(method: .get, url: baseUrl + Packages.rockets, params: [:]).map({ (req, res)  in
            self.parseObjects(json: res, type: Rocket.self)
        })
    }
    
    //MARK: - Helper
   
    func parseObjects<T: Decodable>(json: Any, type:  T.Type) -> [T] {
        if let arr = json as? [[String : Any]] {
            let results = arr.compactMap {
                try? DictionaryDecoder().decode(T.self, from: $0)
            }
            return results
        }
        return []
    }
    
    //MARK: - request
    
    func executeURL(method: HTTPMethod, url: String, params: [String : Any]) -> Observable<(HTTPURLResponse, Any)> {
        return self.manager.rx.responseJSON(method, url, parameters: params)
    }
    
}
