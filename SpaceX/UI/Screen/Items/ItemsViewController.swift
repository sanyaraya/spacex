//
//  ItemsViewController.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemsViewController: UIViewController {

    enum Paths: Int {
        case Rockets
        case Ships
        case Dragons
    }
    
    //MARK: - Properties
    
    @IBOutlet weak var tvContent: UITableView!

    let items = Observable.just(["Rockets", "Ships", "Dragons"])
    let disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "SpaceX items"
        self.bindTable()
    }
    
    //MARK: - Binding
    
    func bindTable() {
        self.items.bind(to: self.tvContent.rx.items(cellIdentifier:"Cell")) {
            table, model, cell in
            cell.textLabel?.text = model
        }.disposed(by: self.disposeBag)
        
        self.tvContent.rx.itemSelected.subscribe(onNext: { (path) in
            self.tvContent.deselectRow(at: path, animated: true)
            
            let loader: Observable<[ItemPresentable]>
            
            switch path.row {
                case Paths.Rockets.rawValue:
                    loader = DataManager.shared.getRockets()
                case Paths.Ships.rawValue:
                    loader = DataManager.shared.getShips()
                case Paths.Dragons.rawValue:
                    loader = DataManager.shared.getDragons()
            default:
                loader = DataManager.shared.getRockets()
            }
            
            let vc = EntitiesViewController.getInstance(loader: loader)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }).disposed(by: self.disposeBag)
    }
}

//MARK: - StoryboardInstantiable

extension ItemsViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Items
    }
}
