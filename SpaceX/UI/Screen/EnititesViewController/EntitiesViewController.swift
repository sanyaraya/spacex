//
//  EntitiesViewController.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EntitiesViewController: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    @IBOutlet weak var tvEntities: UITableView!
    
    var loader: Observable<[ItemPresentable]>?
    
    let disposeBag = DisposeBag()
    
    //MARK: - Instance
    
    static func getInstance(loader: Observable<[ItemPresentable]>) -> EntitiesViewController {
        let vc = EntitiesViewController.storyboardInstance()
        vc?.loader = loader
        return vc!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        
        self.bindTable()
    }
    
    //MARK: - Binding

    private func bindTable() {
        self.tvEntities.delegate = self
        
        self.loader?.do(onNext: { [weak self](_) in
          self?.aiView.stopAnimating()
        }).bind(to: self.tvEntities.rx.items(cellIdentifier:EnitityCell.identifier, cellType: EnitityCell.self)) {
            _, model, cell in
            cell.model = model
            }.disposed(by: self.disposeBag)
        
        self.tvEntities.rx.modelSelected(ItemPresentable.self).subscribe(onNext: {[weak self] (model) in
            visitWeb(webSite: model.infoLink)
            
            if let indexPath = self?.tvEntities.indexPathForSelectedRow {
                self?.tvEntities.deselectRow(at: indexPath, animated: true)
            }
        }).disposed(by: self.disposeBag)
    }
    
    private func registerCell() {
        self.tvEntities.register(UINib(nibName: EnitityCell.nibName, bundle: nil), forCellReuseIdentifier: EnitityCell.identifier)
    }
    
}

//MARK: - UITableViewDelegate

extension EntitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width / 3
    }
}
//MARK: - StoryboardInstantiable

extension EntitiesViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Items
    }
}
