//
//  EnitityCell.swift
//  SpaceX
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EnitityCell: UITableViewCell {
    
    //MARK: - Properties
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivEntity: UIImageView!
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    
    var disposeBag = DisposeBag()
    let loader = ImageLoader()
    
    var model: ItemPresentable? {
        didSet {
            guard let entity = self.model else {
                return
            }
            self.lblTitle.text = entity.title
            self.loadImage(url: entity.imageUrl)
        }
    }
    
    //MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }

    func loadImage(url: String) {
        if let urlReq = URL(string: url) {
            self.aiView.startAnimating()
            self.loader.loadImageUrl(url: urlReq).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](image) in
                if let img = image {
                    self?.ivEntity.image = img
                }
                self?.aiView.stopAnimating()
            }).disposed(by: self.disposeBag)
        }
    }
}

extension EnitityCell: AutoIndentifierCell {}
