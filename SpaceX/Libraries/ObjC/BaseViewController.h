//
//  BaseViewController.h
//  FoamyCoolness
//
//  Created by Александр on 31.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

- (instancetype)initWithStoryboardName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
